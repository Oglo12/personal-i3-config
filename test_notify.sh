#!/bin/bash

msgs[0]="This is a test notification delivered with love! <3"
msgs[1]="You are special!"
msgs[2]="Meow! 󰄛"
msgs[3]="UwU"
msgs[4]="How was your day? UwU"
msgs[5]="Prrrrr, prrrrrrrrrr..."
msgs[6]="This is a beautiful desktop!"
msgs[7]="Passer-by: Wow! How did you make Windows look that good!?"
msgs[8]="Nothing special here, just a notification from me!"
msgs[9]="Enjoy yourself! UwU"
msgs[10]=":-)"
msgs[11]="How are you!? :-D"
msgs[12]="name -> Friendly Cat"

size=${#msgs[@]}
index=$(($RANDOM % $size))

notify-send "The Friendly Cat" "${msgs[$index]}" &
