#!/bin/bash

source ~/.config/i3/config_lang.sh

if [[ $(config_has_line "no_startup_sound") == "no" ]]; then
	play ~/.config/i3/startup.ogg &
fi
