#!/bin/bash

source ~/.config/i3/libs.sh

~/.i3_conf/pre_startup

~/.config/i3/startup_sound.sh &

nitrogen --restore

start-picom

nm-applet &
blueman-applet &

~/.config/i3/polybar/start &
~/.config/i3/dunst/start &

lxsession &

~/.config/i3/conky/start &

brightnessctl --restore

eww-rice daemon

~/.i3_conf/post_startup

~/.config/i3/welcome.sh &

sleep 0.8 && reload-picom &
