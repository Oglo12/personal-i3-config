#!/bin/bash

start-picom () {
	~/.config/i3/picom/start
}

reload-picom () {
	pkill picom > /dev/null 2>&1
	sleep 0.1
	start-picom > /dev/null 2>&1 &
}

opencmd () {
	alacritty -e $*
}

in_vm () {
	if which systemd-detect-virt >/dev/null 2>&1; then
		if [[ $(systemd-detect-virt) != "none" ]]; then
			echo "yes"
		else
			echo "no"
		fi
	else
		echo "no"
	fi
}

reset_wallpaper () {
	~/.config/i3/reset_wallpaper.sh
}

reset_lockscreen () {
	~/.config/i3/reset_lockscreen.sh
}

notify-warning () {
	notify-send "Rice Warning" "$1" -u critical
}

eww-rice () {
	eww --config ~/.config/i3/eww/ $*
}
