# Just My Personal i3wm Config
> How to Install:
  1. Install all required programs.
  2. (Optional) Configure SDDM theme. [Click for tutorial.](https://linuxconfig.org/how-to-customize-the-sddm-display-manager-on-linux)
  3. (Optional) Install Oh My Bash.
  4. Make sure Curl, Bash, and Git are installed.
  5. Run this command: 
  ```
  eval "$(curl https://codeberg.org/Oglo12/personal-i3-config/raw/branch/main/manage/setup_1.sh)"
  ```
  6. Start i3.
  7. Run this command:
  ```
  ~/.config/i3/manage/setup_2.sh
  ```
  8. Configure GTK and cursor theme in LX Appearance.
  9. (Optional) Configure QT theme in Qt5ct if it is installed.

> Out of the Box Requirements:
   0. i3 Window Manager - i3-wm
   1. Rofi - rofi
   2. Picom - picom-pijulius-git
   3. Alacritty - alacritty (can be changed in quick settings)
   4. Brave - brave (can be changed in the dunst config)
   5. Nitrogen - nitrogen
   6. Lib Notify - libnotify
   7. Notification Daemon - notification-daemon
   8. Dunst - dunst
   9. Better Lock Screen - betterlockscreen
  10. Network Manager - networkmanager
  11. Bluez - bluez
  12. Bluez Utils - bluez-utils
  13. Blueman - blueman
  14. Papirus Icon Theme - papirus-icon-theme
  15. Noto Fonts Emojis - noto-fonts-emoji
  16. Polybar - polybar
  17. LXDE Session Polkit - lxsession
  18. Flameshot - flameshot
  19. Conky - conky
  20. Conky Manager - conky-manager
  21. Brightness Control - brightnessctl
  22. Light - light
  23. LX Appearance - lxappearance
  24. YAD - yad
  25. Sox - sox
  26. Eww - eww
  27. Trayer - trayer

> GTK Themes and More to Install (Recommended) (NOTE: The multichoice ones only need you to pick one. But you can use all of them if you want.):
  1. GTK Themes:\
    1. Skeuos GTK Themes - skeuos-gtk\
    2. Mint Themes - mint-themes
  2. Cursor Theme:\
    1. Gruvbox Cursor Themes:
      - Dark Cursor Theme - xcursor-simp1e-gruvbox-dark
      - Light Cursor Theme - xcursor-simp1e-gruvbox-light

> Recommended Programs to Install:
  1. Qt5ct - qt5ct ~ Changing The QT Theme

> Other Optional Programs to Install:
  1. VIS - cli-visualizer ~ See audio waves in the terminal.
  2. TTY Clock - tty-clock-git ~ See the time in the terminal.

> Login Manager (Arch Linux Only):
  1. The Login Manager: SDDM - sddm
  2. Login Manager Theme: Multicolor SDDM Theme - multicolor-sddm-theme

> Other Optional Configurations Used For This Rice:
  1. vis - https://codeberg.org/Oglo12/personal-vis-config/ - ~/.config/vis/

> How to Update your Version of This Rice:
  1. Open a terminal.
  2. Make sure Git is installed.
  3. Run this command:
     ```
     update-rice
     ```
     If that fails, try this command:
     ```
     ~/.config/i3/manage/update.sh
     ```
