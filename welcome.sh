#!/bin/bash

welcome="Welcome App"

source ~/.config/i3/libs.sh

if [[ -f ~/.rice_i3_welcomed ]]; then
	exit
fi

back="Back."

subpage () {
	echo -e "$2\n$back" | rofi -i -dmenu -p "$1:"
}

bool_question () {
	bash ~/.config/rofi/scripts/bool_question.sh "$1"
}

greeted () {
	touch ~/.rice_i3_welcomed
	echo "You will no longer be bothered."
}

welstart () {
	cont="Continue"
	close="Close Welcome Screen"

	proceed=$(echo -e "$cont\n$close" | rofi -i -dmenu -p "Welcome:")

	if [[ $proceed == $cont ]]; then
		if [[ $(bool_question "Update rice?") == true ]]; then
			opencmd bash ~/.config/i3/manage/update.sh --notify
		fi

		sleep 1
		reload-picom
		sleep 1

		notify-send "$welcome" "On the bar above your screen, you will see some buttons and scrollers. Play around! :->"

		greeted

		exit
	elif [[ $proceed == $close ]]; then
		open_again=$(bool_question "Open again?")

		if [[ $open_again == "false" ]]; then
			greeted
		fi

		exit
	fi

	welstart
}

welstart
