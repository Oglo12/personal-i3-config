alias lock='betterlockscreen -l -q'
alias leave='pkill -KILL -u $USER'
alias update-rice='~/.config/i3/manage/update.sh'
alias stop-picom='killall picom'
alias start-picom='~/.config/i3/picom/start'
alias refresh-wallpaper='nitrogen --restore'
alias sbu='~/.config/i3/brightness/plus.sh'
alias sbd='~/.config/i3/brightness/minus.sh'
alias fsm='~/.config/i3/rofi_fs_manager.sh'
alias unlock-welcome='rm ~/.rice_i3_welcomed'

reload-picom () {
	stop-picom
	start-picom
}
