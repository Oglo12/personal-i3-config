#!/bin/bash

yad \
--title "Yad Power Menu" --no-escape --buttons-layout=center --fixed --on-top --center --sticky \
--text "What would you like to do?" \
--button="  Shut Down!system-shutdown!Power off the machine.":"poweroff" \
--button="  Reboot!system-reboot!Reboot the machine.":"reboot" \
--button="  Log Out!system-log-out!Log out of the current session.":"pkill -KILL -u $USER" \
--button="  Lock!system-lock-screen!Lock the machine.":"betterlockscreen -l" \
--button="  Cancel!cancel!Cancel the power menu.":EXIT:0
