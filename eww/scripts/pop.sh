#!/bin/bash

source ~/.config/i3/libs.sh

current_info=$(eww-rice windows | grep "$1")

if [[ $current_info == "*$1" ]]; then
	exit
fi

eww-rice open "$1"
sleep 0.7
eww-rice close "$1"
