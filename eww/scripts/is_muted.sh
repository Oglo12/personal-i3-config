#!/bin/bash

is_muted=$(pactl list sinks | awk '/Mute/ {print $2}')

echo "$is_muted"