#!/bin/bash

conf_dir=$HOME/.i3_conf

mkdir -p $conf_dir

touch $conf_dir/config

config_has_line () {
	success="no"

	for i in $(cat $conf_dir/config); do
		if [[ $i == "#"* ]]; then
			success="no"
		elif [[ $i == $1 ]]; then
			success="yes"
		fi
	done

	echo $success
}
